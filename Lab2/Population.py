import numpy as np
import math

class Population:
    def __init__(self, mi, chromosomeCount, xMin, xMax, fitnessFunction = sum):
        self.mi = mi
        self.chromosomeCount = chromosomeCount
        self.fitnessFunction = fitnessFunction
        self.xMin = xMin
        self.xMax = xMax

    @classmethod
    def random(cls, mi, chromosomeCount, xMin, xMax, fitnessFunction = sum):
        obj = cls(mi, chromosomeCount, xMin, xMax)
        obj.y = np.random.rand(mi, chromosomeCount) * (xMax - xMin) + xMin
        obj.s = np.random.rand(mi, chromosomeCount) * 8
        obj.f = np.apply_along_axis(fitnessFunction, 1, obj.y)
        obj.fitnessFunction = fitnessFunction
        return obj

    @classmethod
    def empty(cls, mi, chromosomeCount, xMin, xMax, fitnessFunction = sum):
        obj = cls(mi, chromosomeCount, xMin, xMax)
        obj.y = np.empty((mi, chromosomeCount))
        obj.s = np.empty((mi, chromosomeCount))
        obj.f = np.empty(mi)
        obj.fitnessFunction = fitnessFunction
        return obj

    @classmethod
    def subPopulation(cls, parentPopulation, indexes):
        obj = cls(len(indexes), parentPopulation.chromosomeCount, parentPopulation.xMin, parentPopulation.xMax)
        obj.y = parentPopulation.y[indexes, :]
        obj.s = parentPopulation.s[indexes, :]
        obj.f = parentPopulation.f[indexes]
        obj.fitnessFunction = parentPopulation.fitnessFunction
        return obj

    @classmethod
    def marriage(cls, parentPopulation, ro):
        selectedIndividuals = np.array(np.random.permutation(parentPopulation.mi))[:ro]
        return Population.subPopulation(parentPopulation, selectedIndividuals)

    @classmethod
    def selection(cls, offsprings, population, includeParents = True):
        if(includeParents):
            offsprings.y = np.vstack((offsprings.y, population.y))
            offsprings.s = np.vstack((offsprings.s, population.s))
            offsprings.f = np.hstack((offsprings.f, population.f))
        indexes = np.argsort(offsprings.f)[:population.mi]
        return Population.subPopulation(offsprings, indexes)

    def yRecombination(self):
        return np.mean(self.y, axis = 0)

    def sRecombination(self):
        return np.mean(self.s, axis = 0)
    
    def mutate(self):
        c = 1
        tau0 = np.exp(c / math.sqrt(2 * self.chromosomeCount) * np.random.randn(self.mi))
        tau = c / (math.sqrt(2) * math.sqrt(self.chromosomeCount))
        
        self.s = (self.s.T * tau0).T * np.exp(tau * np.random.randn(self.mi, self.chromosomeCount))
        self.y += self.s * np.random.randn(self.mi, self.chromosomeCount)
        self.y = np.maximum(self.xMin, np.minimum(self.xMax, self.y))
        self.f = np.apply_along_axis(self.fitnessFunction, 1, self.y)



def evolutionStrategy(ro, lambdaParam, population, includeParents = True, 
    maxGeneration = 1000, expectedValue = 0, epsilon = 0.0001):

    bestFitHistory = []
    avgFitHistory = []

    bestFitHistory.append(np.min(population.f))
    avgFitHistory.append(np.mean(population.f))
        
    generation = 0
    while(generation < maxGeneration and abs(expectedValue - population.f[0]) > epsilon):
        offsprings = Population.empty(lambdaParam, population.chromosomeCount, population.xMin, population.xMax, population.fitnessFunction)
        
        for o in range(0, lambdaParam):
            parents = Population.marriage(population, ro)
            offsprings.y[o] = parents.sRecombination()
            offsprings.s[o] = parents.yRecombination()

        offsprings.mutate()
        population = Population.selection(offsprings, population, includeParents)

        bestFitHistory.append(population.f[0])
        avgFitHistory.append(np.mean(population.f))
        generation += 1

    return population.y[0], population.f[0], generation, bestFitHistory, avgFitHistory        
