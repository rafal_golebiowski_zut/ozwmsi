import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import math
from Population import Population, evolutionStrategy


def griewank(x):
    t1 = (x**2 / 4000).sum()
    t2 = 1
    for i in range(len(x)):
        t2 *= math.cos(x[i]/ math.sqrt(i + 1))      # +1 bo liczymy od 0
    return t1 - t2 + 1

def rosenbrock(x):
    return np.sum(100 * (x[1:] - x[:-1]**2)**2 + (x[:-1] - 1)**2)

def easom(x):
    return -math.cos(x[0]) * math.cos(x[1]) * math.exp(-(x[0] - math.pi)**2 - (x[1] - math.pi)**2)


def ESWithDraw(ro, lambdaParam, population, includeParents,
	generationMax, expectedValue, epsilon, knownGlobalMinimum):
	bestY, bestF, generation, bestFitHistory, avgFitHistory = evolutionStrategy(ro, lambdaParam, population, includeParents,
		generationMax, expectedValue, epsilon)

	print("Generation: ", generation)
	print("BestY: ", bestY)
	print("BestF: ", bestF)

	print("Distance to globalMinimum: ", math.sqrt(np.sum((knownGlobalMinimum - bestY)**2)))

	plt.figure()
	plt.plot(range(len(bestFitHistory)), bestFitHistory)
	plt.plot(range(len(avgFitHistory)), avgFitHistory)

mi = 10    #ilosc osobnikow w populacji
ro = 2     #ilosc osobnikow rodzicielskich
lambdaParam = 100    #ilosc osobnikow potomnych
chromosomeCount = 60
generationMax = 10000
epsilon = 0.0001

fitnessFunction = griewank
expectedValue = 0
xMin = np.repeat(-600, chromosomeCount)
xMax = np.repeat(600, chromosomeCount)
knownGlobalMinimum = np.repeat(0, chromosomeCount)

"""
fitnessFunction = rosenbrock
expectedValue = 0
xMin = np.repeat(-5, chromosomeCount)
xMax = np.repeat(10, chromosomeCount)
knownGlobalMinimum = np.repeat(1, chromosomeCount)
"""

"""
fitnessFunction = easom
chromosomeCount = 2
expectedValue = -1
xMin = np.repeat(-100, chromosomeCount)
xMax = np.repeat(100, chromosomeCount)
knownGlobalMinimum = np.repeat(math.pi, chromosomeCount)
"""

population = Population.random(mi, chromosomeCount, xMin, xMax, fitnessFunction)

#,
ESWithDraw(ro, lambdaParam, population, False,
	generationMax, expectedValue, epsilon, knownGlobalMinimum)

#+
ESWithDraw(ro, lambdaParam, population, True,
	generationMax, expectedValue, epsilon, knownGlobalMinimum)

plt.show()
