import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors as mcolors
import math

from FitnessFunctions import griewank
from FitnessFunctions import rosenbrock
from FitnessFunctions import easom
from ICA import Ica


imperialistColors = ["r", "g", "b", "c", "m", "y", "k", "gold", "lime", "pink", "darkmagenta", "steelblue"]
countryCount = 100
dimensionality = 2
imperialistCount = 10

beta = 2
gamma = math.pi/4 if dimensionality == 2 else 0.25
ksi = 0.1

kMax = 1000
epsilon = 0.001

"""
fitnessFunction = griewank
expectedValue = 0
xMin = np.repeat(-600, dimensionality)
xMax = np.repeat(600, dimensionality)
knownGlobalMinimum = np.repeat(0, dimensionality)
"""


fitnessFunction = rosenbrock
expectedValue = 0
xMin = np.repeat(-5, dimensionality)
xMax = np.repeat(10, dimensionality)
knownGlobalMinimum = np.repeat(1, dimensionality)


"""
fitnessFunction = easom
expectedValue = -1
dimensionality = 2
xMin = np.repeat(-100, dimensionality)
xMax = np.repeat(100, dimensionality)
knownGlobalMinimum = np.repeat(math.pi, dimensionality)
"""



if(imperialistCount > len(imperialistColors)):
    colors = dict(mcolors.BASE_COLORS, **mcolors.CSS4_COLORS)
    imperialistColors = [name for name, color in colors.items()]

ica = Ica(countryCount, dimensionality, imperialistCount, fitnessFunction,
        beta, gamma, ksi,
        xMin, xMax, knownGlobalMinimum, expectedValue, 
        kMax, epsilon,
        imperialistColors, False)

bestFitnessHistory = ica.run(True)

plt.plot(range(len(bestFitnessHistory)), bestFitnessHistory)
plt.show()