import numpy as np
import math

def griewank(x):
    t1 = (x**2 / 4000).sum()
    t2 = 1
    for i in range(len(x)):
        t2 *= math.cos(x[i]/ math.sqrt(i + 1))      # +1 bo liczymy od 0
    return t1 - t2 + 1

def rosenbrock(x):
    return np.sum(100 * (x[1:] - x[:-1]**2)**2 + (x[:-1] - 1)**2)

def easom(x):
    return -math.cos(x[0]) * math.cos(x[1]) * math.exp(-(x[0] - math.pi)**2 - (x[1] - math.pi)**2)
