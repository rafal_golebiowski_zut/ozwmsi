import numpy as np
import matplotlib.pyplot as plt
import math

class Ica:
    def __init__(self, countryCount, dimensionality, imperialistCount, fitnessFunction,
                beta, gamma, ksi, 
                xMin, xMax, knownGlobalMinimum, expectedValue,
                kMax, epsilon,
                imperialistColors, zoom):
        self.countryCount = countryCount
        self.dimensionality = dimensionality
        self.imperialistCount = imperialistCount
        self.fitnessFunction = fitnessFunction
        self.beta = beta
        self.gamma = gamma
        self.ksi = ksi

        self.xMin = xMin
        self.xMax = xMax
        self.knownGlobalMinimum = knownGlobalMinimum
        self.expectedValue = expectedValue

        self.kMax = kMax
        self.epsilon = epsilon

        self.imperialistColors = imperialistColors
        self.zoom = zoom

        self.makeImperialistsAndColonies()
        self.splitColoniesBetweenImperialists()

    def run(self, draw):
        k = 0
        bestFitnessHistory = []
        while((self.imperialistCount > 1) and (k < self.kMax)):
            self.assimilation()
            self.exchangePositions()
            self.updateTotalPower()
            self.imperialistCompetition()
            self.eliminatePowerlessEmpires()
            if(draw == True and self.dimensionality == 2):
                self.plotImperialists()

            bestFit = np.min(self.imperialistCosts)
            bestFitnessHistory.append(bestFit)

            k += 1
            if((bestFit - self.expectedValue) < self.epsilon):
                break


        bestFitIndex = np.argmin(self.imperialistCosts)
        bestImperialist = self.imperialists[bestFitIndex]
        print("bestPosition: ", bestImperialist)
        print("bestFitness: ", bestFit)
        print("k: ", k)

        tmp = self.knownGlobalMinimum - bestImperialist
        print('Distance: ', tmp.dot(tmp)**0.5)
        if(draw == True and self.dimensionality == 2):
            plt.show()
        return bestFitnessHistory

    def makeImperialistsAndColonies(self):
        countries = np.random.uniform(self.xMin, self.xMax, (self.countryCount, self.dimensionality))
        costs = np.apply_along_axis(self.fitnessFunction, 1, countries)

        imperialistIndexes = np.argsort(costs)[:self.imperialistCount]

        self.imperialists = countries[imperialistIndexes, :]
        self.imperialistCosts = costs[imperialistIndexes]

        self.colonies = np.delete(countries, imperialistIndexes, 0)
        self.coloniesCosts = np.delete(costs, imperialistIndexes, None)

    def splitColoniesBetweenImperialists(self):
        normalizedImperialistCosts = self.imperialistCosts - np.max(self.imperialistCosts)
        normalizedPower = np.abs(normalizedImperialistCosts / np.sum(normalizedImperialistCosts))

        splitPoints = np.cumsum(np.round(normalizedPower * self.colonies.shape[0])).astype(int)
        self.imperialistColonies = np.array_split(self.colonies, splitPoints[:-1])
        self.imperialistColoniesCosts = np.array_split(self.coloniesCosts, splitPoints[:-1])

    def assimilation(self):
        for i in range(len(self.imperialists)):
            if(len(self.imperialistColonies[i]) == 0):
                continue
            x = np.random.uniform(0, self.beta, (len(self.imperialistColonies[i]), 1)) * (self.imperialists[i] - self.imperialistColonies[i])
            if(self.dimensionality == 2):
                theta = np.random.uniform(-self.gamma, self.gamma, (len(self.imperialistColonies[i]), 1))
                thetaSin = np.sin(theta)
                thetaCos = np.cos(theta)
                x = np.hstack((x[:, 0][:, np.newaxis] * thetaCos - x[:, 1][:, np.newaxis] * thetaSin,
                    x[:, 0][:, np.newaxis] * thetaSin + x[:, 1][:, np.newaxis] * thetaCos))
            else:
                x += np.random.uniform(-self.gamma * x, self.gamma * x)

            self.imperialistColonies[i] += x
            self.imperialistColoniesCosts[i] = np.apply_along_axis(self.fitnessFunction, 1, self.imperialistColonies[i])

    def exchangePositions(self):
        for i in range(len(self.imperialists)):
            if(len(self.imperialistColonies[i]) == 0):
                continue
            bestColonyIndex = np.argmin(self.imperialistColoniesCosts[i])
            bestColonyCost = self.imperialistColoniesCosts[i][bestColonyIndex]
            if(self.imperialistCosts[i] > bestColonyCost):
                tmpPos = self.imperialists[i]
                tmpCost = self.imperialistCosts[i]

                self.imperialists[i] = self.imperialistColonies[i][bestColonyIndex, :]
                self.imperialistCosts[i] = bestColonyCost

                self.imperialistColonies[i][bestColonyIndex, :] = tmpPos
                self.imperialistColoniesCosts[i][bestColonyIndex] = tmpCost

    def updateTotalPower(self):
        self.totalPower = np.array([self.imperialistCosts[i] + self.ksi * np.mean(self.imperialistColoniesCosts[i][:]) for i in range(len(self.imperialistColonies))])
    
    def imperialistCompetition(self):
        weakestEmpireIndex = np.argmax(self.totalPower)
        if(len(self.imperialistColonies[weakestEmpireIndex]) == 0):
            return
        weakestColonyIndex = np.argmax(self.imperialistColoniesCosts[weakestEmpireIndex][:])
        
        normalizedTotalPower = self.totalPower - np.max(self.totalPower)

        probabilityToTakeWeakestColony = normalizedTotalPower / np.sum(normalizedTotalPower)
        probabilityToTakeWeakestColony -= np.random.uniform(0, 1, (len(probabilityToTakeWeakestColony)))

        winnerIndex = np.argmax(probabilityToTakeWeakestColony)

        if(winnerIndex == weakestColonyIndex):
            return

        self.imperialistColonies[winnerIndex] = np.vstack((self.imperialistColonies[winnerIndex][:], self.imperialistColonies[weakestEmpireIndex][weakestColonyIndex, :]))
        self.imperialistColoniesCosts[winnerIndex] = np.hstack((self.imperialistColoniesCosts[winnerIndex][:], self.imperialistColoniesCosts[weakestEmpireIndex][weakestColonyIndex]))

        self.imperialistColonies[weakestEmpireIndex] = np.delete(self.imperialistColonies[weakestEmpireIndex], weakestColonyIndex, 0)
        self.imperialistColoniesCosts[weakestEmpireIndex] = np.delete(self.imperialistColoniesCosts[weakestEmpireIndex], weakestColonyIndex)

    def eliminatePowerlessEmpires(self):
        imperialistsToEliminate = []
        for i in range(len(self.imperialists)):
            if(len(self.imperialistColonies[i]) == 0):
                imperialistsToEliminate.append(i)
        
        if(len(imperialistsToEliminate) == 0):
            return

        self.imperialists = np.delete(self.imperialists, imperialistsToEliminate, 0)
        self.imperialistCosts = np.delete(self.imperialistCosts, imperialistsToEliminate)
        self.totalPower = np.delete(self.totalPower, imperialistsToEliminate)

        for index in sorted(imperialistsToEliminate, reverse=True):
            del self.imperialistColonies[index]
            del self.imperialistColoniesCosts[index]

        self.imperialistCount -= len(imperialistsToEliminate)
        
    def plotImperialists(self):
        plt.cla()
        if(self.zoom == False):
            plt.xlim(self.xMin[0], self.xMax[0])
            plt.ylim(self.xMin[1], self.xMax[1])
        
        for i in range(len(self.imperialists)):
            plt.plot(self.imperialists[i, 0], self.imperialists[i, 1], "*", color = self.imperialistColors[i], markersize = 25)

            plt.plot(self.imperialistColonies[i][:, 0], self.imperialistColonies[i][:, 1], "o", color = self.imperialistColors[i])
        
        
        plt.pause(0.0001)
