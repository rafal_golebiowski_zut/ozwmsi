import numpy as np
import matplotlib.pyplot as plt
import math

from FitnessFunctions import griewank
from FitnessFunctions import easom
from FitnessFunctions import rosenbrock

from PSO import Pso

swarmSize = 100
omega = 0.9
fiParticle = 0.35
fiSwarm = 0.25

dimensionality = 10
kMax = 2000
epsilon = 0.0001

"""
fitnessFunction = griewank
expectedValue = 0
xMin = np.repeat(-600, dimensionality)
xMax = np.repeat(600, dimensionality)
knownGlobalMinimum = np.repeat(0, dimensionality)


"""
fitnessFunction = rosenbrock
expectedValue = 0
xMin = np.repeat(-5, dimensionality)
xMax = np.repeat(10, dimensionality)
knownGlobalMinimum = np.repeat(1, dimensionality)


"""
fitnessFunction = easom
expectedValue = -1
dimensionality = 2
xMin = np.repeat(-100, dimensionality)
xMax = np.repeat(100, dimensionality)
knownGlobalMinimum = np.repeat(math.pi, dimensionality)
"""

bestPosition, bestFitness, k, bestFitnessHistory = Pso(swarmSize, omega, fiParticle, fiSwarm, 
    dimensionality, fitnessFunction, expectedValue, xMin, xMax, knownGlobalMinimum, 
    kMax, epsilon, True)

plt.plot(range(len(bestFitnessHistory)), bestFitnessHistory)
plt.show()

