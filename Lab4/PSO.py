import numpy as np
import matplotlib.pyplot as plt

def Pso(swarmSize, omega, fiParticle, fiSwarm, 
    dimensionality, fitnessFunction, expectedValue, xMin, xMax, knownGlobalMinimum,
    kMax, epsilon, draw = True):

    particlePositions = np.random.uniform(xMin, xMax, (swarmSize, dimensionality))
    particleBestPositions = np.copy(particlePositions)

    particleVelocities = np.random.uniform(size = (swarmSize, dimensionality))

    particleFitnesses = np.apply_along_axis(fitnessFunction, 1, particlePositions)
    particleBestFitnesses = np.copy(particleFitnesses)

    bestParticleindex = np.argmin(particleBestFitnesses)
    swarmBestPosition = np.copy(particleBestPositions[bestParticleindex, :])
    swarmBestFitness = particleBestFitnesses[bestParticleindex]

    bestFitnessHistory = [swarmBestFitness]

    for k in range(kMax):
        particleVelocities = (omega * particleVelocities + 
            fiParticle * np.random.uniform(0, particleBestPositions - particlePositions) +
            fiSwarm * np.random.uniform(0, swarmBestPosition - particlePositions))

        particlePositions += particleVelocities

        outOfBoundIndexes = np.any((particlePositions < xMin) + (particlePositions > xMax), axis = 1)
        #print(particlePositions[outOfBoundIndexes])
        particlePositions[outOfBoundIndexes, :] = np.random.uniform(xMin, xMax, (outOfBoundIndexes.sum(), dimensionality))
        particleVelocities[outOfBoundIndexes, :] = np.random.uniform(size = (outOfBoundIndexes.sum(), dimensionality))
        
        particleFitnesses = np.apply_along_axis(fitnessFunction, 1, particlePositions)

        indexesWithBetterFitness = particleFitnesses < particleBestFitnesses
        particleBestPositions[indexesWithBetterFitness] = np.copy(particlePositions[indexesWithBetterFitness])
        particleBestFitnesses[indexesWithBetterFitness] = np.copy(particleFitnesses[indexesWithBetterFitness])

        bestParticleindex = np.argmin(particleBestFitnesses)
        if(particleBestFitnesses[bestParticleindex] < swarmBestFitness):
            swarmBestPosition = np.copy(particleBestPositions[bestParticleindex, :])
            # print(f'{k}. {swarmBestFitness} -> {particleBestFitnesses[bestParticleindex]}')
            swarmBestFitness = particleBestFitnesses[bestParticleindex]

        bestFitnessHistory.append(swarmBestFitness)

        if(swarmBestFitness - expectedValue < epsilon):
            break

        if(draw == True and dimensionality == 2):
            plt.cla()
            plt.plot(particleBestPositions[:, 0], particleBestPositions[:, 1], "g*")
            plt.scatter(particlePositions[:, 0], particlePositions[:, 1])
            plt.plot(swarmBestPosition[0], swarmBestPosition[1], "r*")
            plt.plot(knownGlobalMinimum[0], knownGlobalMinimum[1], "k*")
            plt.xlim(xMin[0], xMax[0])
            plt.ylim(xMin[1], xMax[1])
            
            plt.pause(0.0001)

    print("bestPosition: ", swarmBestPosition)
    print("bestFitness: ", swarmBestFitness)
    print("k: ", k)

    tmp = knownGlobalMinimum - swarmBestPosition
    print('Distance: ', tmp.dot(tmp)**0.5)
    if(draw == True and dimensionality == 2):
        plt.show()
    return swarmBestPosition, swarmBestFitness, k, bestFitnessHistory
